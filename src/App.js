import React from "react";
// import ReactDOM from "react-dom/client";

class App extends React.Component {
  state = {
    a: 21,
    b: 23,
    // res: null,
    res: 0,
    meter: 0,
    stepLength: 1,
  };

  Add = () => this.setState((state) => (state.res += state.a + state.b));
  Multiple = () => this.setState((state) => (state.res = state.a * state.b));
  Divide = () => this.setState((state) => (state.res = state.a / state.b));
  Minus = () => this.setState((state) => (state.res = state.a - state.b));
  yurish = () =>
    this.setState((state) => (state.meter = state.meter + state.stepLength));
  step = () =>
    this.setState((state) => (state.stepLength = state.stepLength += 1));

  render() {
    const { a, b, res, meter, stepLength } = this.state;
    return (
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <h1 style={{ color: "white" }}>a = {a}</h1>
        <h1 style={{ color: "white" }}>b = {b}</h1>
        <h1 style={{ color: "white" }}>Result: {res}</h1>
        <main style={{ display: "flex", gap: "10px" }}>
          <button
            onClick={this.Add}
            style={{
              border: "2px solid purple",
              padding: "15px 25px",
              borderRadius: "10px",
              background: "transparent",
              cursor: "pointer",
              color: "white",
            }}
          >
            a+b
          </button>
          <button
            onClick={this.Multiple}
            style={{
              border: "2px solid purple",
              padding: "15px 25px",
              borderRadius: "10px",
              background: "transparent",
              cursor: "pointer",
              color: "white",
            }}
          >
            a*b
          </button>
          <button
            onClick={this.Divide}
            style={{
              border: "2px solid purple",
              padding: "15px 25px",
              borderRadius: "10px",
              background: "transparent",
              cursor: "pointer",
              color: "white",
            }}
          >
            a/b
          </button>
          <button
            onClick={this.Minus}
            style={{
              border: "2px solid purple",
              padding: "15px 25px",
              borderRadius: "10px",
              background: "transparent",
              cursor: "pointer",
              color: "white",
            }}
          >
            a-b
          </button>
        </main>
        <h1 style={{ color: "white" }}>{meter} metr</h1>
        <button
          onClick={this.yurish}
          style={{
            border: "2px solid purple",
            padding: "15px 25px",
            borderRadius: "10px",
            background: "transparent",
            cursor: "pointer",
            color: "white",
          }}
        >
          yurish
        </button>
        <h1 style={{ color: "white" }}>Qadam kattaligi: {stepLength} metr</h1>

        <button
          onClick={this.step}
          style={{
            border: "2px solid purple",
            padding: "15px 25px",
            borderRadius: "10px",
            background: "transparent",
            cursor: "pointer",
            color: "white",
          }}
        >
          Qadamni kengaytirish
        </button>
      </div>
    );
  }
}

export default App;
