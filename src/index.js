import React from "react";
import ReactDOM from "react-dom/client";

import App from "./App";

ReactDOM.createRoot(document.getElementById("root")).render(<App />);

// import './index.css';
// import App from './App';
// import reportWebVitals from './reportWebVitals';

// const root = ReactDOM.createRoot(document.getElementById('root'));
// root.render(
//   <React.StrictMode>
//     <App />
//   </React.StrictMode>
// );

// // If you want to start measuring performance in your app, pass a function
// // to log results (for example: reportWebVitals(console.log))
// // or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();

// class App extends React.Component {
//   state = {
//     name: "ali",
//     age: 18,
//     lname: "valiyev",
//     gender: "male",
//     count: 4,
//   };

//   Add = () => this.setState((state) => (state.count = state.count += 1));
//   Multiple = () => this.setState((state) => (state.count = state.count *= 2));
//   Divide = () => this.setState((state) => (state.count = state.count /= 2));
//   Minus = () => this.setState((state) => (state.count = state.count -= 1));

//   // login = () => {
//   //   console.log(this);
//   // };
//   // logout = () => {
//   //   console.log(this);
//   // };
//   // signIn = () => {
//   //   console.log(this);
//   // };
//   // logMessage = () => {
//   //   console.log(this);
//   // };
//   render() {
//     const { name, lname, gender, age, count } = this.state;
//     return (
//       <>
//         <header>header</header>
//         <main>
//           <button onClick={this.Add}>Add1</button>
//           <button onClick={this.Multiple}>Multiple</button>
//           <button onClick={this.Divide}>Divide</button>
//           <button onClick={this.Minus}>Minus</button>
//         </main>

//         <h1>{name}</h1>
//         <h1>{lname}</h1>
//         <h1>{gender}</h1>
//         <h1>{age}</h1>
//         <h1>{count}</h1>
//       </>
//     );
//   }
// }
